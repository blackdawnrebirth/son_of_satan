﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public float speed;
    private Rigidbody2D rb;

//    private int direction = 0; //  1 up 2 right 3 down 4 left
    enum direction { up,down,left,right,not};
    private direction movingDirection;



    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        movingDirection = direction.not;
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        rb.constraints = RigidbodyConstraints2D.FreezeRotation;

        switch (movingDirection)
        {
            case direction.up:
            {
 //                   rb.constraints = RigidbodyConstraints2D.FreezePositionX;
                    rb.velocity = new Vector2(0,speed * Time.fixedDeltaTime);
                    break;    
            }
            case direction.down:
                {
 //                   rb.constraints = RigidbodyConstraints2D.FreezePositionX;
                    rb.velocity = new Vector2(0, -speed * Time.fixedDeltaTime);
                    break;
                }
            case direction.left:
                {
//                    rb.constraints = RigidbodyConstraints2D.FreezePositionY;
                    rb.velocity = new Vector2( -speed * Time.fixedDeltaTime,0);
                    break;
                }
            case direction.right:
                {
 //                   rb.constraints = RigidbodyConstraints2D.FreezePositionY;
                    rb.velocity = new Vector2(speed * Time.fixedDeltaTime,0);
                    break;
                }
            case direction.not:
                {

                    break; }

        }

    }

    private void Update()
    {
        //If Not moving see if moving
        if (movingDirection==direction.not) { PlayerMovement(); }

        CheckHitWall();
//        CheckCollsions();
        rb.rotation = 0f;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag=="killer")
        {
            string currentSceneName = SceneManager.GetActiveScene().name;
            SceneManager.LoadScene(currentSceneName);
        }
    }


    void CheckHitWall()
    {
        int layer_mask = LayerMask.GetMask("Wall");
        switch (movingDirection)
        {
        case direction.up:
                if (Physics2D.Raycast(transform.position, transform.TransformDirection(Vector2.up), 1 ,layer_mask))
                {
                    movingDirection = direction.not;
                }
            break;
        case direction.right:
            if (Physics2D.Raycast(transform.position, transform.TransformDirection(Vector2.right), 1, layer_mask))
            {
                    movingDirection = direction.not;
                }
            break;
        case direction.down:
                if (Physics2D.Raycast(transform.position, transform.TransformDirection(Vector2.down), 1, layer_mask))
            {
                    movingDirection = direction.not;
                }
            break;
        case direction.left:
                if (Physics2D.Raycast(transform.position, transform.TransformDirection(Vector2.left), 1, layer_mask))
            {
                    movingDirection = direction.not;
                }
            break;

    }
    }    
    
    void PlayerMovement()
    {

        if (Input.GetKey(KeyCode.A))
        {
            movingDirection = direction.left;
  //          rb.constraints = RigidbodyConstraints2D.FreezePositionY;
            return;
        }

        if (Input.GetKey(KeyCode.D))
        {
            movingDirection = direction.right;
  //         rb.constraints = RigidbodyConstraints2D.FreezePositionY;
            return;
        }

       if (Input.GetKey(KeyCode.W))
        {
            movingDirection = direction.up;
 //           rb.constraints = RigidbodyConstraints2D.FreezePositionX;
            return;
        }

        if (Input.GetKey(KeyCode.S))
        {
            movingDirection = direction.down;
 //           rb.constraints = RigidbodyConstraints2D.FreezePositionX;
            return;
        }

    }
}
