﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;




public class MonsterUD : MonoBehaviour
{
    public float speed;
    private bool goingUp = false;
    Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();   
    }

    // Update is called once per frame
    void Update()
    {
        int layer_mask = LayerMask.GetMask("Wall");
        if (goingUp == true)
        {
            if (Physics2D.Raycast(transform.position, transform.TransformDirection(Vector2.up), 0.6f, layer_mask))
            { goingUp = false; }
            rb.velocity = new Vector2(0, speed * Time.fixedDeltaTime);
        }
        else
        {
            if (Physics2D.Raycast(transform.position, transform.TransformDirection(Vector2.down), 0.6f, layer_mask))
            { goingUp = true; }
            rb.velocity = new Vector2(0, -speed * Time.fixedDeltaTime);
        }
    }
}
