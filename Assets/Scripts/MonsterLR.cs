﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;




public class MonsterLR : MonoBehaviour
{
    public float speed;
    private bool goingRight = false;
    Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();   
    }

    // Update is called once per frame
    void Update()
    {
        int layer_mask = LayerMask.GetMask("Wall");
        if (goingRight == true)
        {
            if (Physics2D.Raycast(transform.position, transform.TransformDirection(Vector2.right), 0.6f, layer_mask))
            { goingRight = false; }
            rb.velocity = new Vector2(speed * Time.fixedDeltaTime,0);
        }
        else
        {
            if (Physics2D.Raycast(transform.position, transform.TransformDirection(Vector2.left), 0.6f, layer_mask))
            { goingRight = true; }
            rb.velocity = new Vector2(-speed * Time.fixedDeltaTime,0);
        }
    }
}
