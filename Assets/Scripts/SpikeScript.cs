﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeScript : MonoBehaviour
{
    private float timeLeft;
    private float startTime;
    private bool showing = false;
    Collider2D sc;
    Sprite sprite;
    // Start is called before the first frame update
    void Start()
    {
        startTime = 4f;

    }

    // Update is called once per frame
    void Update()
    {
        timeLeft -= Time.deltaTime;
        if(timeLeft<0)
        {
            //reset time and change showing or not
            timeLeft = startTime;
            showing = !showing;
            if (showing == true)
                {
                GetComponent<SpriteRenderer>().enabled = true;
                GetComponent<Collider2D>().enabled = true;
                }
            else
                {
                GetComponent<SpriteRenderer>().enabled = false;
                GetComponent<Collider2D>().enabled = false;
            }

        }
    }
}
